using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TracingPointsManager : MonoBehaviour
{
    // public bool MUST_DONE_ORDERLY = false;
    public List<TracingGroup> tg;
    public LineDrawer lrPrefab;

    public int currentTg = 0;
    public int numOfLr = 0;

    void Awake() {
        SubscribeChildren();
        RenewLineDrawer();
    }

    public void SubscribeChildren() {
        int i = 1;
        foreach(Transform child in transform) {
            if (child.GetComponent<TracingGroup>()) {
                TracingGroup _tg = child.GetComponent<TracingGroup>();
                _tg.myTPM = this;
                _tg.order = i;
                _tg.gameObject.SetActive(false);
                tg.Add(_tg);
                i++;
            }
        }
    }

    public void RenewLineDrawer() {
        currentTg++;
        if (numOfLr < tg.Count) {
            LineDrawer ld = Instantiate(lrPrefab, tg[numOfLr].transform).transform.GetComponent<LineDrawer>();
            tg[numOfLr].gameObject.SetActive(true);
            tg[numOfLr].ld = ld;
            numOfLr++;
        }
    }
}
