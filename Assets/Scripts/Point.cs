﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    public bool hasBeenHit = false;
    public TracingGroup myTg;

    public void SetHasBeenHit(bool h) {
        hasBeenHit = h;
        GetComponent<Collider2D>().enabled = !h;
    }
}
