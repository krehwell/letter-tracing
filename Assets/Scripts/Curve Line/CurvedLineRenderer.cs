using System.Collections.Generic;
using UnityEngine;

public class CurvedLineRenderer : MonoBehaviour
{
    [Tooltip("Tolerance for line renderer to be simplified")]
    public float tolerance = .15f;
    
    [Tooltip("Size of line segments (in meters) used to approximate the curve")]
    public float lineSegmentSize = 2f;

    [Tooltip("Width of the line (initial width if useCustomEndWidth is true)")]
    public float lineWidth = 0.1f;

    [Tooltip("Enable this to set a custom width for the line end")]
    public bool useCustomEndWidth = false;

    [Tooltip("Custom width for the line end")]
    public float endWidth = 0.1f;

    public void SetPointsToLine(LineRenderer lineRenderer, Vector3[] linePositions) {
        // get smoothed values
        Vector3[] smoothedPoints = LineSmoother.SmoothLine(linePositions, lineSegmentSize);

        // set line settings
        lineRenderer.positionCount = smoothedPoints.Length;
        lineRenderer.SetPositions(smoothedPoints);
        lineRenderer.startWidth = lineWidth;
        lineRenderer.endWidth = useCustomEndWidth ? endWidth : lineWidth;
    }
}
