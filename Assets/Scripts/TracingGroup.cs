using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TracingGroup : MonoBehaviour
{
    // public bool ALLOW_DRAG_FROM_ANY_POINTS = false;
    public TracingPointsManager myTPM;
    public int order;

    public List<Point> points;
    public int iPointShouldBeHit = 0;

    public bool userHasHitWrongPoint = false;
    public LineDrawer ld;

    void Awake() {
        SubscribeChildren();
    }

    public void SubscribeChildren() {
        foreach(Transform child in transform) {
            Point _point = child.GetComponent<Point>();
            if (_point) {
                _point.myTg = this;
                points.Add(_point);
            }
        }
    }
    
    void Update() {
        if (myTPM.currentTg == order) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
            MouseHitPointDetection(ray, hit);
            MouseHitWrongPointDetection(ray, hit);
        }
    }

    public void MouseHitPointDetection(Ray ray, RaycastHit2D hit) {
        if (Input.GetMouseButton(0)) {
           
            var p = hit.collider?.GetComponent<Point>(); 
            if (p && AllowToHitNextPoint(p.transform)) {
                IncreamentToNextPointAction(p);
            }
        }

        if (Input.GetMouseButtonUp(0)) {
            if (!ShouldLineDrawerStay()) {
                ResetAllPoints();
                ld.ClearLine();
            } else {
                ld.FinishOneLineAction();                
                myTPM.RenewLineDrawer();
            }
        }
    }

    public void MouseHitWrongPointDetection(Ray ray, RaycastHit2D hit) {
        if (Input.GetMouseButton(0)) {
            var p = hit.collider?.GetComponent<WrongPoint>(); 
            if (p) {
                userHasHitWrongPoint = true;
            }
        }
    }

    public bool ShouldLineDrawerStay() {
        foreach(Point p in points) {
            if (!p.hasBeenHit || userHasHitWrongPoint) {
                return false;
            }
        }
        return true;
    }

    public void ResetAllPoints() {
        foreach(Point p in points) {
            p.SetHasBeenHit(false);
        }
        iPointShouldBeHit = 0;
        userHasHitWrongPoint = false;
    }

    public void IncreamentToNextPointAction(Point p) {
        p.SetHasBeenHit(true);
        int numOfPoints = points.Count;
        if (iPointShouldBeHit < numOfPoints) {
            iPointShouldBeHit++;
        }
    }

    public bool AllowToHitNextPoint(Transform currentPointHit) {
        try {
            return Object.Equals(currentPointHit, points[iPointShouldBeHit].transform);
        } catch { return false; }
    }
}
