﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    public float MINIMUM_DIST_TO_DRAW_NEW_POINT = .3f;

    public LineRenderer lr;
    public List<Vector2> lPos;
    public bool stopDrawing = false;

    public CurvedLineRenderer clr;

    void Start() {
        lPos.Add(Vector2.zero);
    }

    void Update() {
        if (!stopDrawing) {
            ReadInput();
        } 
    }

    public void ReadInput() {
        if (Input.GetMouseButton(0)) {
            if (IsAllowAddNewPoint()) {
                AddNewInputPos();
                DrawNewLine();
            }
        }
    }

    public void SimplifyLine() {
        if (Input.GetMouseButtonUp(0)) {
            if (lr.positionCount > 0) {
                lr.Simplify(clr.tolerance);
                List<Vector3> newPointPositions = new List<Vector3>();
                for (int i = 0; i < lr.positionCount; i++) {
                    newPointPositions.Add(lr.GetPosition(i));
                }
                clr.SetPointsToLine(lr, newPointPositions.ToArray());
            }
        }
    }
    
    public void FinishOneLineAction() {
        SimplifyLine();
        stopDrawing = true;
    }

    public bool IsAllowAddNewPoint() {
        Vector2 currentFingerPos = GetPointByInput();

        if (Vector2.Distance(currentFingerPos, GetLatestPointPos()) > MINIMUM_DIST_TO_DRAW_NEW_POINT) {
            return true;
        }

        return false;
    }

    public void AddNewInputPos() {
        lPos.Add(GetPointByInput());
    }

    public void DrawNewLine() {
        int nextPosition = lr.positionCount++;
        lr.SetPosition(nextPosition, GetLatestPointPos());
    }

    public void ClearLine() {
        lr.positionCount = 0;
        lPos.Clear();
        lPos.Add(Vector2.zero);
    }

    public Vector2 GetLatestPointPos() {
        return lPos[lPos.Count - 1];
    }

    public Vector2 GetPointByInput() {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
